"use strict";
function validForm(formId) {
    let form = document.getElementById(formId);
    let divlist = form.getElementsByTagName('div');
    for (let x = 0; x < divlist.length ;x++){
        let divInput = divlist[x].getElementsByTagName('input')[0];
        let name = divInput.name;
        name = name.charAt(0).toUpperCase() + name.slice(1);
        let value = divInput.value;
        let regex = new RegExp(divInput.pattern);

        if (value === '' || regex.exec(value) === null ){
            divlist[x].getElementsByTagName('p')[0].innerHTML = name + ' Invalide ';
        }
        else{
            divlist[x].getElementsByTagName('p')[0].innerHTML = '';
        }
    }
}