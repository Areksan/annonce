function updateRubrique(valLib) {
    let Libelle = prompt('Nouveau Libelle de : ' + valLib);
    if (Libelle == null || Libelle == '') {
        return false;
    } else {
        let hiddenLibelle = document.getElementById('update' + valLib);
        hiddenLibelle.setAttribute('value', Libelle);
        return true;
    }
}

function validDelete(valLib) {
    let userReturn = prompt('Pour valider taper exactement : ' + valLib);
    if (userReturn === valLib) {
        return true;
    } else {
        return false;
    }
}