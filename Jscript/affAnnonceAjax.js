let rub = document.getElementById("rubrique");
rub.addEventListener("change", test);

//Sans JSON
function test() {
    let url = 'main.php?action=afficherAnnonces&rub=' + rub.value;
    let httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                let txtHtml = httpRequest.responseText;
                let minidom = document.createElement('html');
                minidom.innerHTML = txtHtml;
                console.log(txtHtml);
                let AnnonceList = minidom.getElementsByClassName('Annonce');
                let testo = document.getElementsByClassName('jsaipa');
                let testochild = testo[0].lastElementChild;
                while (testochild) {
                    testo[0].removeChild(testochild);
                    testochild = testo[0].lastElementChild;
                }
                while (AnnonceList.length != 0) {
                    testo[0].appendChild(AnnonceList[0]);
                }
            }
        }
    };
    httpRequest.open('GET', url);
    httpRequest.send();
}

//Avec Json
function test2() {
    let url = "main.php?action=afficherAnnoncesAjax&rub=" + rub.value;
    let httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', url);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onload = function () {
        let annonces = httpRequest.response;

        let testo = document.getElementsByClassName('jsaipa');
        let testochild = testo[0].lastElementChild;
        while (testochild) {
            testo[0].removeChild(testochild);
            testochild = testo[0].lastElementChild;
        }
        for (let i = 0; i < annonces.length; i++) {
            annonceTab = annonces[i]['Annonce'];

            let art = document.createElement('article');
            let corps = document.createElement('div');
            let ImgContent = document.createElement('div');
            let Content = document.createElement('div');
            let endart = document.createElement('span');


            art.setAttribute('class', 'Annonce');
            corps.setAttribute('class', 'Corps');
            ImgContent.setAttribute('class', 'ImgContent');
            Content.setAttribute('class', 'Content');

            let link = "main.php?action=afficherAnnonces&ann=" + annonceTab['id'];

            let imglink = document.createElement('a');
            imglink.setAttribute('href', link);

            let img = document.createElement('img');
            img.setAttribute('src', '../' + annonceTab['Img']['Img']['Path']);

            imglink.appendChild(img);
            ImgContent.appendChild(imglink);

            let tete = document.createElement('h3');
            tete.textContent = annonceTab['Tete'];
            let contentcorps = document.createElement('p');
            contentcorps.textContent = annonceTab['Corps'];

            Content.appendChild(tete);
            Content.appendChild(contentcorps);

            corps.appendChild(ImgContent);
            corps.appendChild(Content);

            let endartLink = document.createElement('a');
            endartLink.setAttribute('href', link);
            let endartImg = document.createElement('img');
            endartImg.setAttribute('src', '../Public/Img/Add.png');
            let dFin = document.createElement('p');
            let dDeb = document.createElement('p');
            let libel = document.createElement('p');
            let ann = document.createElement('p');
            let annlink = document.createElement('a');
            dFin.textContent = 'Date Fin :' + annonceTab['End'];
            dDeb.textContent = 'Date Debut :' + annonceTab['Start'];
            libel.textContent = annonceTab['Rubrique']['rubrique']['Libelle'];
            annlink.textContent = "Annonceur : " + annonceTab['User']['User']['Pseudo'];
            annlink.setAttribute('href', 'main.php?action=profile&val=' + annonceTab['User']['User']['Pseudo']);
            ann.appendChild(annlink);

            endartLink.appendChild(endartImg);
            endart.appendChild(endartLink);
            endart.appendChild(dFin);
            endart.appendChild(dDeb);
            endart.appendChild(libel);

            endart.appendChild(ann);
            art.appendChild(corps);
            art.appendChild(endart);

            testo[0].appendChild(art);
        }
    }
}