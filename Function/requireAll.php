<?php
function requireAll($filename, $dir)
{
    $file = scandir($dir);
    foreach ($file as $item) {
        $ext = explode('.', $item);
        if ($ext[1] === 'php' && $ext[0] != $filename) {
            require_once(realpath($dir.'/'.$item));
        }
    }
}