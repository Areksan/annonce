<?php

trait MysqlTrait
{


    protected function insertObject(object $object): Object
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $returnTab = $this->objectValidation($object);
        $process = $cnx->prepare("CALL Creer_" . $returnTab[0] . "(" . $returnTab[1] . ")");
        $process->execute($returnTab[2]);
        return $this->returnNewObject($object);
    }

    protected function deleteObject(object $object): int
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $returnTab = $this->objectValidation($object);
        try {
            $process = $cnx->prepare("CALL Del_" . $returnTab[0] . "(?)");
            $process->execute(array($returnTab[2][0]));
            $count = $process->rowCount();
            return $count;
        } catch (\PDOException $e) {
            echo($e->getMessage() . "\n");
            echo((int)$e->getCode() . "\n");
            return null;
        }
    }

    protected function updateObject(object $object): int
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $returnTab = $this->objectValidation($object);
        try {
            $process = $cnx->prepare("CALL Update_" . $returnTab[0] . "(" . $returnTab[1] . ")");
            $process->execute($returnTab[2]);
            $count = $process->rowCount();
            return $count;
        } catch (\PDOException $e) {
            echo($e->getMessage() . "\n");
            echo((int)$e->getCode() . "\n");
            return null;
        }
    }

    protected function getAllObject($object_type, $tab, $mod = null)
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $process = $cnx->prepare("CALL getAll_" . $object_type . "()");
        $process->execute();
        if ($mod == null) {
            $data = $process->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $object_type, $tab);
        } elseif ($mod == 'O') {
            $data = $process->fetchAll();
        } else {
            throw new Exception('Mod invalide');
        }
        return $data;
    }

    protected function getByIdObject($object_type, $id, $tab, $mod = null)
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $process = $cnx->prepare("CALL getById_" . $object_type . "(?)");
        $process->execute(array($id));
        if ($mod == null) {
            $data = $process->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $object_type, $tab);
            if (isset($data[0]) == null) {
                throw new Exception($object_type . ' Inexistant');
            } else {
                return $data[0];
            }
        } elseif ($mod == 'O') {
            $data = $process->fetchAll();
            if (count($data) === 0) {
                throw new Exception($object_type . ' Inexistant');
            } else {
                return $data;
            }
        } else {
            throw new Exception('Mod invalide');
        }
    }

    private function returnNewObject(Object $object)
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $saveid = $cnx->prepare("SELECT LAST_INSERT_ID();");
        $saveid->execute();
        $saveid = $saveid->fetch(PDO::FETCH_COLUMN);
        $object->setId($saveid);
        return clone $object;
    }

    private function objectValidation(object $object): array
    {
        $object_type = get_class($object);
        if (class_exists($object_type)) {
            $methods = get_class_methods($object_type);
            $getfunction = preg_grep('#^[g][e][t]#', $methods);
            $getfunction = preg_grep('#^[g][e][t][^_]#', $getfunction);
            $dot = '';
            $tab = [];
            foreach ($getfunction as $item) {
                $dot .= '?,';
                $tab[] = $object->$item();
            }
        } else {
            throw new Exception('Class Invalide');
        }
        $dot = substr($dot, 0, strlen($dot) - 1);
        return array($object_type, $dot, $tab);
    }

}