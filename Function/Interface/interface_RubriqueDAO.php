<?php
interface RubriqueDAO
{
    function insert(Rubrique $rubrique) :Rubrique;

    function delete(Rubrique $rubrique): int;

    function update(Rubrique $rubrique): int;

    function getAll(): array;
}