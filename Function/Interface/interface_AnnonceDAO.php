<?php
interface AnnonceDAO
{
    function insert(Annonce $annonce) : Annonce ;

    function delete(Annonce $annonce): int;

    function update(Annonce $annonce): int;

    function getByRubriqueId(Rubrique $rubrique): array ;

    function getByUser(Utilisateur $utilisateur): array;

    function deletePerimees(): int;
}