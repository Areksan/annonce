<?php
interface UtilisateurDAO
{
    function insert(Utilisateur $user) :Utilisateur;

    function identifier(Utilisateur $user);
}