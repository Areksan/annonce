<?php
require_once(realpath(__DIR__ . '/../Function/requireAll.php'));
require_once(realpath(__DIR__ . '/Function/Main_Cont/Main_Cont_All.php'));
require_once(realpath(__DIR__ . '/../Class/AllClass.php'));
require_once(realpath(__DIR__ . '/../vendor/autoload.php'));
session_start();
if (isset($_GET['action']) != null) {
    switch ($_GET['action']) {
        case 'Identification' :
            identification();
            break;
        case 'afficherAnnonces' :
            afficherAnnonces();
            break;
        case 'afficherAnnoncesAjax' :
            afficherAnnoncesAjax();
            break;
        case 'ajouterAnnonce':
            ajouterAnnonce();
            break;
        case 'administration':
            administration();
            break;
        case 'profile' :
            profile();
            break;
        default:
            error404();
            break;
    }
} else {
    afficherRubriques();
}