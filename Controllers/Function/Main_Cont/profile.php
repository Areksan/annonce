<?php
function profile(){
    try {
        $twigclass = new TwigClass();
        $cnxAnnonce = new MysqlAnnonce();
        $user = new Utilisateur($_GET['val'], 0, 0, 0, 0);
        $data = $cnxAnnonce->getByUser($user);
        $twigclass->rendu('ListeAnnonce.html.twig', ['annonceListe' => $data, 'Rubrique' => $_GET['val']]);
    }
    catch (Exception $exception){
        $twigclass->errorRender($exception);
    }
}