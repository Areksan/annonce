<?php
function identification()
{
    try {
        $twigClass = new TwigClass();
        if (isset($_POST['Pseudo']) != null || isset($_POST['PseudoInsc']) != null || isset($_SESSION['Pseudo'])) {
            $userCnx = new MysqlUser();
            if (isset($_POST['Pseudo']) != null) {
                $user = new Utilisateur($_POST['Pseudo'], '0', '0', '0', $_POST['Mdp']);
                $test = $userCnx->identifier($user);
                if ($test == $_POST['Pseudo']) {
                    $_SESSION['Pseudo'] = $test;
                    $_SESSION['IdRole'] = $userCnx->getById($test)->getIdRole();
                    header('Location: ' . $_SERVER['PHP_SELF']);
                    exit;
                } else {
                    $twigClass->rendu('Connexion.html.twig');
                }

            }
            if (isset($_SESSION['Pseudo']) != null) {
                unset($_SESSION['Pseudo']);
                header('Location: ' . $_SERVER['PHP_SELF']);
                exit;
            }
            if (isset($_POST['PseudoInsc']) != null) {
                $user = new Utilisateur($_POST['PseudoInsc'], $_POST['Nom'], $_POST['Prenom'], $_POST['Mail'], password_hash($_POST['MdpInsc'], PASSWORD_DEFAULT));
                $test = $userCnx->insert($user);
                $_SESSION['Pseudo'] = $test->getId();
                header('Location: ' . $_SERVER['PHP_SELF']);
                exit;
            }
        } else {
            $twigClass->rendu('Connexion.html.twig');
        }
    }catch (Exception $exception){
        $twigClass->rendu('Connexion.html.twig',['error'=>$exception->getMessage()]);
    }
}
