<?php
function administration()
{
    $twigClass = new TwigClass();
    try{
    if (isset($_SESSION['Pseudo']) != null) {
        $cnxUser = new MysqlUser();
        $role = $cnxUser->getById($_SESSION['Pseudo'])->getIdRole();
        $verif =  $role === 1 || $role === 2 ;
        if ( $verif ) {
            header('location: admin.php');
            exit();
        }
        else{
            throw new Exception("Vous n'avez pas les droits d'accés");
        }
    }
    else{
        throw new Exception('Veuillez vous connectez');
    }
    }catch (Exception $exception){
        $twigClass->errorRender($exception);
    }
}