<?php
function ajouterAnnonce()
{
    $twigClass = new TwigClass();
    $cnxAnnonce = new MysqlAnnonce();
    $cnxRubrique = new MysqlRubrique();
    $cnxUser = new MysqlUser();
    $nbPost = null;
    foreach ($_POST as $item) {
        $nbPost .= $item;
    }
    if (isset($_SESSION['Pseudo']) != null) {
        $listeRubrique = $cnxRubrique->getAll();
        $twigClass->rendu('AjouterAnnonce.Html.twig', ["Rubriques" => $listeRubrique]);
        if (isset($nbPost) != null && (count($_POST) === 5 || count($_POST) === 6)) {
            $rubrique = $cnxRubrique->getById($_POST['Rubrique']);
            $user = $cnxUser->getById($_SESSION['Pseudo']);
            if ($_FILES['img']['size'] === 0) {
                $annonce = new Annonce(0, $rubrique, $user, $_POST["Titre"], $_POST['Description'], $_POST['Mise_en_Ligne'], $_POST['Fin_d\'Annonce'], new Img(1, ''));
            } else {
                $img = addImg($_FILES['img']);
                $annonce = new Annonce(0, $rubrique, $user, $_POST["Titre"], $_POST['Description'], $_POST['Mise_en_Ligne'], $_POST['Fin_d\'Annonce'], $img);
            }
            $newannonce = $cnxAnnonce->insert($annonce);
            header('location:' . $_SERVER['PHP_SELF'] . '?action=afficherAnnonces&ann=' . $newannonce->getId());
            exit();
        }
    } else {
        $twigClass->rendu('Erreur.html.twig', ['error' => 'Veuillez vous connectez']);
    }
}

function addImg($img)
{
    if ($img['size'] > 100000) {
        throw new Exception('Le fichier à une taille supérieur à 100Ko');
    }
    switch ($img['type']) {
        case "image/jpeg":
            $type = '.jpeg';
            break;
        case "image/png":
            $type = '.png';
            break;
        default:
            throw new Exception('Le fichier n\'est pas une image jpeg ou png');
            break;
    }
    $chemin = __DIR__ . "/../../../Public/userimg/" . $_SESSION['Pseudo'];
    $name = '/img' . (count(scandir($chemin)) - 2);
    $x = rand(0, 9);
    if (is_dir($chemin) != true) {
        mkdir($chemin);
    }
    while (file_exists($chemin . $name) == true) {
        $name .= $x;
        $x = rand(0, 9);
    }
    $name .= $type;
    move_uploaded_file($img['tmp_name'], $chemin . $name);
    $cnximg = new MysqlImg();
    return $cnximg->insert(new Img(0, "/Public/userimg/" . $_SESSION['Pseudo'] . $name));
}

function rejexDate()
{
//TODO : créer une fonction générant des rejex selon la date actuel.
    $actualDate = date('d/m/Y');
    $futurDate = date('d/m/Y', mktime(0, 0, 0, date('m') + 10, date('d') + 6, date('Y')));
    $tabActualDate = explode('/', $actualDate);
    print rejexReturn($tabActualDate);
    $tabFuturDate = explode('/', $futurDate);
    print PHP_EOL;
    print rejexReturn($tabFuturDate);
}

function rejexReturn(array $tab)
{
    $dateRegex = '';
    if ($tab[0] < 10) {
        $dateRegex .= "^((0)[" . $tab[0][1] . "-9]|[1-2][0-9]|(3)[0-1])";
    } elseif ($tab[0] < 20) {
        $dateRegex .= "^((1)[" . $tab[0][1] . "-9]|(2)[0-9]|(3)[0-1])";
    }
    $dateRegex .= "(\/)";
    $nextMonth = $tab[1][1] + 1;
    if ($tab[1] < 10) {
        $dateRegex .= "(0)(" . $tab[1][1] . ")";
        $dateRegexMonth = '((0)[' . $nextMonth . "-9]|(1)[0-2])";
    } elseif ($tab[1] > 10) {
        $dateRegex .= "(1)(" . $tab[1][1] . ")";
        $dateRegexMonth = '((1)[' . $nextMonth . "-2])";
    }
    $dateRegex .= "(\/)(".$tab[2].")$". "|^([0-2][0-9]|(3)[0-1])(\/)" . $dateRegexMonth ."(\/)(". $tab[2].")$";
    $dateRegex .= "|^([0-2][0-9]|(3)[0-1])(\/)((0)[1-9]|(1)[0-2])(\/)(".($tab[2]+1).")$";
    return $dateRegex;
}
//rejexDate();