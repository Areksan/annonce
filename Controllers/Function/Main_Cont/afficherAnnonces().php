<?php
function afficherAnnonces()
{
    try {
        $cnxAnnonce = new MysqlAnnonce();
        $cnxRubrique = new MysqlRubrique();
        $twigClass = new TwigClass();
        if (isset($_GET['rub']) != null && isset($_GET['ann']) == null) {
            if ($_GET['rub'] == ''){throw new Exception('404 Page Not Found');}
            $rubrique = $cnxRubrique->getById($_GET['rub']);
            $listeRubriques = $cnxRubrique->getAll();
            $listeAnnonce = $cnxAnnonce->getByRubriqueId($rubrique);
            $twigClass->rendu('ListeAnnonce.html.twig', ["Rubrique" => $rubrique->getLibelle(), "annonceListe" => $listeAnnonce,"rubs"=>$listeRubriques]);
        }
        if (isset($_GET['ann']) != null && isset($_GET['rub']) == null) {
            if ($_GET['ann'] == ''){throw new Exception('404 Page Not Found');}
            $annonce = $cnxAnnonce->getById($_GET['ann']);
            $content = $annonce[0];
            $twigClass->rendu('Annonce.html.twig', ['content' => $content]);
        }
    } catch (Exception $exception) {
        $twigClass->errorRender($exception);
    }
}
function afficherAnnoncesAjax()
{
    try {
        $cnxAnnonce = new MysqlAnnonce();
        $cnxRubrique = new MysqlRubrique();
        $twigClass = new TwigClass();
        if (isset($_GET['rub']) != null && isset($_GET['ann']) == null) {
            if ($_GET['rub'] == ''){throw new Exception('404 Page Not Found');}
            $rubrique = $cnxRubrique->getById($_GET['rub']);
            $listeAnnonce = $cnxAnnonce->getByRubriqueId($rubrique);
            $lx = null ;
            foreach ($listeAnnonce as $item) {
                $lx[] = $item->jsonSerialize();
            }
            echo json_encode($lx);
        }
    } catch (Exception $exception) {
        $twigClass->errorRender($exception);
    }
}