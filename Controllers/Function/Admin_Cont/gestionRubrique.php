<?php
function gestionRubrique()
{
    try {
        $twigClass = new TwigClass();
        $cnxRu = new MysqlRubrique();
        if (isset($_POST['libelle']) != null) {
            $ru = new Rubrique($_POST['libelle']);
            $cnxRu->insert($ru);
        } elseif (isset($_POST['del'])) {
            $rub = new Rubrique('0', $_POST['del']);
            $cnxRu->delete($rub);
        } elseif (isset($_POST['update'])) {
            $rub = new Rubrique($_POST['update-Lib'], $_POST['update-Id']);
            $cnxRu->update($rub);
        }
        $twigClass->rendu('AddRubriques.html.twig', ['data' => $cnxRu->getAll()]);
    } catch (Exception $exception) {
        $twigClass->errorRender($exception);
    }
}