<?php
function gestionUser(){
    $twigClass = new TwigClass();
    $cnxUser = new MysqlUser();
    $cnxRole = new MysqlRole();
    if (isset($_POST['Sup'])){
        $user = new Utilisateur($_POST['Sup'],0,0,0,0);
        $cnxUser-> delete($user);
    }
    if (isset($_POST['Mod'])){
        $user = new Utilisateur($_POST['Pse'],0,0,0,0,$_POST['Mod']);
        $cnxUser->editRole($user);
    }
    $twigClass->rendu('gestUtilisateur',['users'=>$cnxUser->getAll(),'role'=>$cnxRole->getAll()]);
}