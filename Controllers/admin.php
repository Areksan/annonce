<?php
require_once(realpath(__DIR__ . '/../Function/requireAll.php'));
require_once(realpath(__DIR__ . '/Function/Admin_Cont/Admin_Cont_All.php'));
require_once(realpath(__DIR__ . '/../Class/AllClass.php'));
require_once(realpath(__DIR__ . '/../vendor/autoload.php'));
session_start();
$twigclass = new TwigClass();
try {
    if (isset($_SESSION['Pseudo']) != null) {
        $cnxUser = new MysqlUser();
        $role = $cnxUser->getById($_SESSION['Pseudo'])->getIdRole();
        $verif = $role === 1 || $role === 2;
        if ($verif) {
            if (isset($_GET['action'])) {
                switch ($_GET['action']) {
                    case 'gestionUser':
                        gestionUser();
                        break;
                    case 'gestionAnnonce':
                        gestionAnnonce();
                        break;
                    case 'gestionRubrique' :
                        gestionRubrique();
                        break;
                    case 'gestionRole':
                        print 'lol';
                        break;
                    default :
                        throw new Exception('404 - Page Not Found');
                        break;
                }
            } else {
                $twigclass->rendu('Adminpannel.html.twig');
            }
        } else {
            throw new Exception("Vous n'avez pas les droits d'accés");
        }
    } else {
        throw new Exception('Veuillez vous connectez');
    }
} catch (Exception $exception) {
    $twigclass->errorRender($exception);
}