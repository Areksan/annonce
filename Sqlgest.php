<?php
class Sqlgest{
    private static $cnx ;
    private static $instance = null ;
    private $host,$db,$user,$pass,$charset ,$dsn,$options,$limdate ;

    private function __construct(){
        $host = '127.0.0.1';
        $db = 'annonce';
        $user = 'root';
        $pass = '';
        $charset = 'utf8';
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this -> limdate = new DateInterval('P3W');
        try{
            self::$cnx = new PDO($dsn,$user,$pass,$options) ;
        }
        catch (Exception $e){
            $e->getMessage();
        }
    }

    public static function getConnexion(){
        if(!self::$instance){
            self::$instance = new Sqlgest();
        }
        return self::$instance;
    }

    function getCnx(){
        return self::$cnx ;
    }
}
