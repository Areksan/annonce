<?php


class Img implements JsonSerializable
{
    private $ID_IMG,$PATH;
    public function __construct($ID_IMG,$PATH)
    {
        $this->ID_IMG = $ID_IMG;
        $this->PATH = $PATH;
    }

    /**
     * @return mixed
     */
    public function getIDIMG()
    {
        return $this->ID_IMG;
    }

    /**
     * @return mixed
     */
    public function getPATH()
    {
        return $this->PATH;
    }

    /**
     * @param mixed $ID_IMG
     */
    public function setID($ID_IMG): void
    {
        $this->ID_IMG = $ID_IMG;
    }

    /**
     * @param mixed $PATH
     */
    public function setPATH($PATH): void
    {
        $this->PATH = $PATH;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'Img'=>[
                'Id'=>$this->getIDIMG(),
                'Path'=>$this->getPATH()
            ]
        ];
    }
}