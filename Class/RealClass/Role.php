<?php

class Role implements JsonSerializable {
    private $ID_ROLE,$NAME_ROLE,$DESCRIPTION_ROLE;
    function __construct($libelle,$description='',$id=null){
        $this ->ID_ROLE = $id ;
        $this ->NAME_ROLE = $libelle ;
        $this ->DESCRIPTION_ROLE = $description ;
    }
    
    function __toString(){
        return '[ '.$this->ID_ROLE.' , '.$this->NAME_ROLE.' , '.$this->DESCRIPTION_ROLE.' ]' ;
    }

    function getNAMEROLE(){return $this-> NAME_ROLE;}
    function getDESCRIPTIONROLE(){return $this->DESCRIPTION_ROLE;}
    function getID_ROLE(){return $this-> ID_ROLE;}
    function setID_ROLE($ID_ROLE){$this->ID_ROLE = $ID_ROLE;}
    function setNAMEROLE($NAME_ROLE){$this -> NAME_ROLE = $NAME_ROLE ;}
    function setDESCRIPTIONROLE($DESCRIPTION_ROLE){$this-> DESCRIPTION_ROLE = $DESCRIPTION_ROLE ;}

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'Role'=>[
                'id'=>$this->getID_ROLE(),
                'Name'=>$this->getNAMEROLE(),
                'Desc'=>$this->getDESCRIPTIONROLE()
            ]
        ];
    }
}

?>