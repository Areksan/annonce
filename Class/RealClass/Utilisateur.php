<?php
class Utilisateur implements JsonSerializable {
    private $PSEUDO_USER,$NOM,$PRENOM,$MAIL,$ID_ROLE,$MDP_USER;
    function __construct($PSEUDO_USER,$NOM,$PRENOM,$MAIL,$MDP_USER,$ID_ROLE=3){
        $this -> PSEUDO_USER = $PSEUDO_USER;
        $this -> NOM = $NOM;
        $this -> PRENOM = $PRENOM;
        $this -> MAIL = $MAIL ;
        $this -> ID_ROLE = $ID_ROLE;
        $this -> MDP_USER =  $MDP_USER;
    }
    
    function __toString(){
        return '[ '.$this->PSEUDO_USER.' , '.$this->NOM.' , '.$this->PRENOM.' , '.$this-> MAIL.','.$this->ID_ROLE.' ]'.PHP_EOL;
    }
    
    function getId(){return $this->PSEUDO_USER  ;}
    function getNom(){return $this-> NOM ;}
    function getPrenom(){return $this-> PRENOM ;}
    function getMail(){return $this-> MAIL ;}
    function getMdp(){return $this-> MDP_USER ;}
    function getIdRole(){return $this-> ID_ROLE ;}

    function setNom($nom){$this ->NOM = $nom ;}
    function setPrenom($prenom){$this ->PRENOM = $prenom ;}
    function setMail($mail){$this ->MAIL = $mail ;}
    function setRole($role){$this ->ID_ROLE = $role ;}
    function setId(){return null;}

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
          'User' => [
              'Pseudo'=>$this->getId(),
              'Nom' => $this->getNom(),
              'Prenom'=>$this->getPrenom(),
              'Mail' =>$this->getMail(),
              'Id Role'=>$this->getIdRole(),
          ]
        ];
    }
}
?>

