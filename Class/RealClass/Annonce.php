<?php
class Annonce implements JsonSerializable {
    private $ID_ANNONCE,$ID_RUBRIQUE,$PSEUDO_USER,$EN_TETE_ANNONCE,$CORPS_ANNONCE,$DATE_ONLINE_ANNONCE,$DATE_LIM_ANNONCE,$ID_IMG;
    public function __construct($ID_ANNONCE,Rubrique $RUBRIQUE,Utilisateur $USER, $EN_TETE_ANNONCE, $CORPS_ANNONCE, $DATE_ONLINE_ANNONCE, $DATE_LIM_ANNONCE, Img $ID_IMG ){
        $this->ID_ANNONCE =$ID_ANNONCE ;
        $this->ID_RUBRIQUE = $RUBRIQUE;
        $this->PSEUDO_USER = $USER;
        $this->EN_TETE_ANNONCE =$EN_TETE_ANNONCE ;
        $this->CORPS_ANNONCE =$CORPS_ANNONCE ;
        $this->DATE_ONLINE_ANNONCE =$DATE_ONLINE_ANNONCE ;
        $this->DATE_LIM_ANNONCE =$DATE_LIM_ANNONCE ;
        $this->ID_IMG = $ID_IMG;
    }
    function __toString(){
        return '[ '.$this->ID_ANNONCE.' , '.$this->ID_RUBRIQUE->getLibelle().' , '.$this->PSEUDO_USER->getId().' , '.$this->EN_TETE_ANNONCE .' , '.$this->CORPS_ANNONCE.' , '.$this->DATE_ONLINE_ANNONCE.' , '.$this->DATE_LIM_ANNONCE.' ]'.PHP_EOL ;
    }

    function getId(){return $this ->ID_ANNONCE;}
    function getIdRubrique(){return $this ->ID_RUBRIQUE->getId() ;}
    function get_LibelleRubrique(){return $this-> ID_RUBRIQUE->getLibelle();}
    function getIdPseudo(){return $this ->PSEUDO_USER->getId() ;}
    function getTete(){return $this ->EN_TETE_ANNONCE ;}
    function getCorps(){return $this ->CORPS_ANNONCE ;}
    function getOnline(){return $this ->DATE_ONLINE_ANNONCE ;}
    function getfin(){return $this ->DATE_LIM_ANNONCE;}
    function getIdImage(){return $this->ID_IMG->getIDIMG();}
    function get_PathImage(){return $this->ID_IMG->getPATH();}

    function setId($id){$this ->ID_ANNONCE = $id ;}
    function setIDRubrique(Rubrique $rubrique){$this ->ID_RUBRIQUE = $rubrique ;}
    function setPseudoUser(Utilisateur $User){$this ->PSEUDO_USER = $User;}
    function setTete($tete){$this ->EN_TETE_ANNONCE = $tete ;}
    function setCorps($corps){$this ->CORPS_ANNONCE = $corps ;}
    function setOnline($online){$this ->DATE_ONLINE_ANNONCE = $online ;}
    function setFin($fin){$this ->DATE_LIM_ANNONCE = $fin ;}
    function setIdImage($idImage){$this->ID_IMG;}

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'Annonce' => [
                'id'=>$this->getId(),
                'Rubrique'=>$this->ID_RUBRIQUE->jsonSerialize(),
                'User' => $this->PSEUDO_USER->jsonSerialize(),
                'Tete'=> $this->getTete(),
                'Corps' => $this->getCorps(),
                'Start' => $this->getOnline(),
                'End' => $this->getfin(),
                'Img' => $this->ID_IMG->jsonSerialize()
            ]
        ];
    }
}
?>
