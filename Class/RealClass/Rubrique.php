<?php

class Rubrique implements JsonSerializable {
    private $ID_RUBRIQUE,$LIBELLE_RUBRIQUE;
    
    function __construct($LIBELLE_RUBRIQUE,$ID_RUBRIQUE = -1 ){
        $this ->ID_RUBRIQUE = $ID_RUBRIQUE;
        $this ->LIBELLE_RUBRIQUE = $LIBELLE_RUBRIQUE;
    }
    
    function __toString(){
        return '[ '.$this->ID_RUBRIQUE.' , '.$this->LIBELLE_RUBRIQUE.' ]' ;
    }
    
    function getId(){return $this-> ID_RUBRIQUE;}
    function getLibelle(){return $this-> LIBELLE_RUBRIQUE;}
    function setLibelle($libelle){$this -> LIBELLE_RUBRIQUE = $libelle ;}
    function setId($id){$this ->ID_RUBRIQUE = $id;}

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'rubrique' => [
                'id' => $this->getId(),
                'Libelle' => $this->getLibelle()
            ]
        ];
    }
}

?>