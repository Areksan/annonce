<?php
require_once(realpath(__DIR__ . '/../../Function/AllFunction.php'));
require_once(realpath(__DIR__ . '../../AllClass.php'));
require_once(realpath(__DIR__ . '../../../Sqlgest.php'));

class MysqlAnnonce implements AnnonceDAO
{
    use MysqlTrait;

    function insert(Annonce $annonce): Annonce
    {
        return $this->insertObject($annonce);
    }

    function update(Annonce $annonce): int
    {
        return $this->updateObject($annonce);
    }

    function delete(Annonce $annonce): int
    {
        try {
            return $this->deleteObject($annonce);
        } catch (PDOException $exception) {
            throw new Exception('Annonce impossible à supprimer.');
        }
    }

    function getAll()
    {
        return $this->dataReturn($this->getAllObject('Annonce', [], 'O'));
    }

    function getById($id)
    {
        return $this->dataReturn($this->getByIdObject('Annonce', $id, [], 'O'));
    }

    function getByRubriqueId(Rubrique $rubrique): array
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $id = $rubrique->getId();
        $process = $cnx->prepare("CALL Annonce_By_Rubrique(?)");
        $process->execute(array($id));
        $data = $process->fetchAll();
        return $this->dataReturn($data);
    }

    function getByUser(Utilisateur $utilisateur): array
    {
        $cnx = Sqlgest::getConnexion()->getCnx();
        $id = $utilisateur->getId();
        $process = $cnx->prepare("CALL Annonce_By_Utilisateur(?)");
        $process->execute(array($id));
        $data = $process->fetchAll();
        if (!isset($data[0])) {
            throw new Exception('Utilisateur inexistant');
        }
        return $this->dataReturn($data);
    }

    function deletePerimees(): int
    {
        try {
            $cnx = Sqlgest::getConnexion()->getCnx();
            $process = $cnx->prepare("CALL Delete_Perimees()");
            $process->execute();
            $count = $process->rowCount();
            return $count;

        } catch (\PDOException $e) {
            echo($e->getMessage() . "\n");
            echo((int)$e->getCode() . "\n");
        }
    }

    function updateAnnonceRubrique(Annonce $annonce)
    {
        try {
            $cnx = Sqlgest::getConnexion()->getCnx();
            $process = $cnx->prepare("CALL Update_Annonce_Rubrique(?,?)");
            $process->execute($annonce->getId(), $annonce->getIdRubrique());
            $count = $process->rowCount();
            return $count;

        } catch (\PDOException $e) {
            echo($e->getMessage() . "\n");
            echo((int)$e->getCode() . "\n");
        }
    }

    private function dataReturn(array $data)
    {
        $dataReturn = [];
        foreach ($data as $item) {
            $dataReturn[] =
                new Annonce(
                    $item["ID_ANNONCE"],
                    new Rubrique($item["LIBELLE_RUBRIQUE"], $item["ID_RUBRIQUE"]),
                    new Utilisateur($item["PSEUDO_USER"], $item["NOM"], $item["PRENOM"], $item["MAIL"], $item["MDP_USER"], $item["ID_ROLE"]),
                    $item["EN_TETE_ANNONCE"],
                    $item["CORPS_ANNONCE"],
                    $item["DATE_ONLINE_ANNONCE"],
                    $item["DATE_LIM_ANNONCE"],
                    new Img($item["ID_IMG"],$item["PATH"]));
        }
        return $dataReturn;
    }
}