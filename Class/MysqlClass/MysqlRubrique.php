<?php
require_once(realpath(__DIR__ . '../../../Function/AllFunction.php'));
require_once(realpath(__DIR__ . '../../AllClass.php'));
require_once(realpath(__DIR__ . '../../../Sqlgest.php'));

class MysqlRubrique implements RubriqueDAO
{
    use MysqlTrait;

    function insert(Rubrique $rubrique): Rubrique
    {
        try {
            return $this->insertObject($rubrique);
        } catch (PDOException $exception) {
            throw new Exception('Rubrique déjà existante !');
        }
    }

    function delete(Rubrique $rubrique): int
    {
        return $this->deleteObject($rubrique);
    }

    function update(Rubrique $rubrique): int
    {
        return $this->updateObject($rubrique);
    }

    function getAll(): array
    {
        return $this->getAllObject('Rubrique',[null,null,null]);
    }

    function getById($id)
    {
        return $this->getByIdObject('Rubrique',$id,[null,null]);
    }
}