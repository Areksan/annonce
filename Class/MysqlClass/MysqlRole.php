<?php

require_once(realpath(__DIR__ . '../../../Function/AllFunction.php'));
require_once(realpath(__DIR__ . '../../AllClass.php'));
require_once(realpath(__DIR__ . '../../../Sqlgest.php'));

class MysqlRole
{
    use MysqlTrait;

    function insert(Role $role)
    {
        try {
            return $this->insertObject($role);
        } catch (PDOException $PDOException) {
            throw new Exception($PDOException->getMessage());
        }
    }

    function delete(Role $role)
    {
        try {
            return $this->deleteObject($role);
        } catch (PDOException $PDOException) {
            throw new Exception($PDOException);
        }
    }

    function update(Role $role)
    {
        try {
            return $this->updateObject($role);
        } catch (PDOException $PDOException) {
            throw new Exception($PDOException);
        }
    }

    function getAll(){
        return $this->getAllObject("Role",[0,0,0]);
    }
}