<?php

require_once(realpath(__DIR__.'../../../Function/AllFunction.php'));
require_once(realpath(__DIR__.'../../AllClass.php'));
require_once(realpath(__DIR__.'../../../Sqlgest.php'));

class MysqlUser implements UtilisateurDAO
{
    use MysqlTrait ;
    function insert(Utilisateur $user): Utilisateur
    {
        return $this->insertObject($user);
    }
    function getAll(){
        return $this->getAllObject("Utilisateur",[null,null,null,null,'0',null]);
    }
    function getById($id){
        return $this->getByIdObject('Utilisateur',$id,[null,null,null,null,null,null]);
    }

    function delete($id){
        return $this->deleteObject($id);
    }

    function identifier(Utilisateur $user)
    {
        try{
            $cnx = Sqlgest::getConnexion()->getCnx();
            $process = $cnx->prepare("CALL Identifier_Utilisateur(?,?)");
            $process -> execute(array($user->getId(),$user->getMDP()));
            $data = $process -> fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'Utilisateur',[null,null,null,null,null,null]);
            if (!isset($data[0])){
                throw new Exception('Identifiant Invalide');
            }
            if (password_verify($user->getMdp(),$data[0]->getMdp()) == false){
                throw new Exception('Identifiant Invalide');
            }
            else{
                return $data[0]->getId();}
        }
        catch (\PDOException $e){
            echo($e->getMessage()."\n");
            echo ((int)$e->getCode()."\n");
        }
    }
    function editRole(Utilisateur $user){
        $cnx = Sqlgest::getConnexion()->getCnx();
        $process = $cnx->prepare('CALL User_edit_Role(?,?)');
        $process->execute(array($user->getId(),$user->getIdRole()));
    }
}