<?php
require_once(realpath(__DIR__ . '/../../Function/AllFunction.php'));
require_once(realpath(__DIR__ . '../../AllClass.php'));
require_once(realpath(__DIR__ . '../../../Sqlgest.php'));

class MysqlImg
{
    use MysqlTrait;
    function insert(Img $img){
        return $this->insertObject($img);
    }

    function delete(Img $img){
        $this->deleteObject($img);
    }

    function getById($id){
        $this->getByIdObject('Img',$id,[null,null]);
    }
}