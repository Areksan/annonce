<?php


class TwigClass
{
    private $loader, $twig, $function;

    public function __construct()
    {
        $dirToTemplates = realpath(__DIR__ . '/../../Templates');
        $templateTab = [$dirToTemplates,$dirToTemplates.'/mini',$dirToTemplates.'/MainTemplate',$dirToTemplates.'/AdmTemplate'];
        $this->loader = new \Twig\Loader\FilesystemLoader($templateTab);
        $this->twig = $twig = new Twig\Environment($this->loader);
        $this->function = new Twig\TwigFunction('returnRoot', function ($filepath) {
            $path = getcwd();
            $neo = explode('annoncePHP', $path);
            $returnRoot = count(explode('\\', $neo[1]));
            $returnDot = '';
            for ($x = 0; $x < $returnRoot - 1; $x++) {
                $returnDot .= '../';
            }
            return $returnDot . $filepath;
        });
        $this->twig->addFunction($this->function);
        $this->twig->addGlobal('url', $_SERVER['PHP_SELF']);
        $this->twig->addGlobal('urlaction', $_SERVER['PHP_SELF'] . '?action=');
        $this->twig->addGlobal('session',$_SESSION);
    }

    function rendu($template, $tableau=[])
    {
        print $this->twig->render($template, $tableau);
    }

    function errorRender($except){
        print $this->rendu('Erreur.html.twig',['error'=>$except->getMessage()]);
        header('Refresh: 3;url=main.php');
    }
}